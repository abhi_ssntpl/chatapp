//
//  ContactClass.swift
//  chatApp
//
//  Created by Sword Software on 03/04/18.
//  Copyright © 2018 Sword Software. All rights reserved.
//

import Foundation
class Contact
{
    var name : String = ""
    var email : String = ""
    var contactID : String = ""
    var lastMessage : String = ""
    var lastMessageTimeStamp : String = ""
    var lastMessageIsRead : String = ""
    var isLastMessageSent : String = ""
    var unreadMessageCount : Int = 0

}
