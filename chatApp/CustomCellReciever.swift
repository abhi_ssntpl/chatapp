//
//  CustomCellReciever.swift
//  chatApp
//
//  Created by Sword Software on 05/04/18.
//  Copyright © 2018 Sword Software. All rights reserved.
//

import UIKit

class CustomCellReciever: UITableViewCell {
    @IBOutlet var messageReceiver: UILabel!
    @IBOutlet var messageSender: UILabel!
    @IBOutlet var timeReciever: UILabel!
    @IBOutlet var timeSender: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
