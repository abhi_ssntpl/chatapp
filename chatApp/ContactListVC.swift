//
//  ContactListVC.swift
//  chatApp
//
//  Created by Sword Software on 03/04/18.
//  Copyright © 2018 Sword Software. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

var Timestamp: TimeInterval {
    return Date().timeIntervalSince1970 * 1000
}
class ContactListVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating,UISearchBarDelegate {
    static var UUID : String = ""
    static var currentDateString : String = ""
    var rootRef : DatabaseReference!
    var messageHandler : DatabaseHandle?
    var selectedContactID : String = ""
    var selectedContactName : String = ""
    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchBarView: UIView!
    var resultSearchController = UISearchController()
    var userContacts = [Contact]()
    var filteredUserContacts = [Contact]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("ContactListVC")
        self.title = "Chats"
        //ContactListVC.UUID = "hYCsrT6u7oWM9fZqe5sjTGZqIoe2"
        let kUserDefault = UserDefaults.standard
        ContactListVC.UUID = kUserDefault.string(forKey: "userUUID")!
        print(ContactListVC.UUID)
        var resultSearchController = UISearchController()
        let date = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        ContactListVC.currentDateString = dateFormatter.string(from: date as Date)
        
        rootRef = Database.database().reference()
        if Reachability.isConnectedToNetwork() {
            self.fetchingContactsFromFirebase()
        }
        
        resultSearchController.searchResultsUpdater = self
        
        resultSearchController.dimsBackgroundDuringPresentation = false
        resultSearchController.searchResultsUpdater = self
        
        
        definesPresentationContext = true
        // tableView.tableHeaderView = resultSearchController.searchBar
        //  searchBarView.addSubview(resultSearchController.searchBar)
        resultSearchController.searchBar.sizeToFit()
        resultSearchController.hidesNavigationBarDuringPresentation = false
        
        
        self.resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.searchBar.backgroundColor = UIColor.blue
            controller.searchBar.barTintColor = UIColor(red: 18.0/255.0, green: 146.0/255.0, blue: 254.0/255.0, alpha: 1.0)
            controller.searchBar.placeholder = "Search Contacts"
            controller.searchBar.tintColor = UIColor.white
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            
            //  self.tableView.tableHeaderView = controller.searchBar
            searchBarView.addSubview(controller.searchBar)
            return controller
        })()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        UserDetails.shared.arrangeIn()
        self.userContacts = UserDetails.shared.contacts
        self.filteredUserContacts = UserDetails.shared.contacts
        print(UserDetails.shared.contacts.count)
        print(self.userContacts.count)
        self.tableView.reloadData()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        //        filteredTableData.removeAll(keepingCapacity: false)
        //        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchController.searchBar.text!)
        //        let arrayName = UserDetails.shared.filtered(using: searchPredicate)
        //        filteredTableData = arrayName as! [String]
        //        self.tableView.reloadData()
        
        
        self.filteredUserContacts = (searchController.searchBar.text!.isEmpty) ? self.userContacts : self.userContacts.filter({ contact -> Bool in
            
            return contact.name.range(of: searchController.searchBar.text!, options: .caseInsensitive) != nil
            
        })
        
        for contact in filteredUserContacts {
            print(contact.name)
            print(contact.email)
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0;//Choose your custom row height
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedContactID = filteredUserContacts[indexPath.row].contactID
        print(self.selectedContactID)
        self.selectedContactName = filteredUserContacts[indexPath.row].name
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        self.performSegue(withIdentifier: "showChats", sender: self)
    }
    
    func fetchingContactsFromFirebase() {
        AppManager.shared.showActivityIndicator(withTitle: "loading contacts")
        rootRef.child("Users").observe(.value, with: {(snapshot : DataSnapshot) in
            print("snapshotsnapshotsnapshot=\(snapshot)")
            if snapshot.exists() {
                for con in snapshot.children {
                    var contact : Contact = Contact()
                    contact.contactID = (con as! DataSnapshot).key as String
                if contact.contactID  != ContactListVC.UUID {
                    
                    if let name = ((con as! DataSnapshot).value as! NSDictionary).value(forKey: "name") {
                        contact.name = name as! String
                    }
                    self.getTotalNumberOfUnreadMessages(contactID : contact.contactID, completionBlock: {(success) -> Void in
                        print("success=\(success)")
                        contact.unreadMessageCount = success
                        
                        self.getLastMessageWithTimeStamp(contactID: contact.contactID, completionBlock: {(success) -> Void in
                            if let timeStamp = success["lastMessageTimeStamp"] {
                                contact.lastMessageTimeStamp = timeStamp as! String
                            }
                            if let lastMessage = success["lastMessage"] {
                                print("lastMessage=\(lastMessage)")
                                contact.lastMessage = lastMessage as! String
                            }
                            if let isRead = success["isRead"] as? String {
                                contact.lastMessageIsRead = isRead as! String
                            }
                            if let isRead = success["isRead"] as? NSNumber {
                                contact.lastMessageIsRead = String(describing: isRead)
                            }
                            if let isLastMessageSent = success["isLastMessageSent"] as? String {
                                contact.isLastMessageSent = isLastMessageSent
                            }
                            let itemExists = UserDetails.shared.contacts.contains(where: { $0.name == contact.name })
                            print(itemExists)
                            if itemExists == false {
                                UserDetails.shared.contacts.append(contact)
                            }
                            
                            UserDetails.shared.arrangeIn()
                            self.userContacts = UserDetails.shared.contacts
                            self.filteredUserContacts = self.userContacts
                            self.tableView.reloadData()
                        })
                    })
//                    self.getLastMessageWithTimeStamp(contactID: contact.contactID, completionBlock: {(success) -> Void in
//                        if let timeStamp = success["lastMessageTimeStamp"] {
//                            contact.lastMessageTimeStamp = timeStamp as! String
//                        }
//                        if let lastMessage = success["lastMessage"] {
//                            print("lastMessage=\(lastMessage)")
//                            contact.lastMessage = lastMessage as! String
//                         }
//                        if let isRead = success["isRead"] as? String {
//                            contact.lastMessageIsRead = isRead as! String
//                        }
//                        if let isRead = success["isRead"] as? NSNumber {
//                            contact.lastMessageIsRead = String(describing: isRead)
//                        }
//                        if let isLastMessageSent = success["isLastMessageSent"] as? String {
//                            contact.isLastMessageSent = isLastMessageSent
//                        }
//                        let itemExists = UserDetails.shared.contacts.contains(where: { $0.name == contact.name })
//                        print(itemExists)
//                        if itemExists == false {
//                            UserDetails.shared.contacts.append(contact)
//                        }
//
//                        UserDetails.shared.arrangeIn()
//                        self.userContacts = UserDetails.shared.contacts
//                        self.filteredUserContacts = self.userContacts
//                       self.tableView.reloadData()
//                    })
                }
                else {
                    UserDetails.shared.id = (con as! DataSnapshot).key
                    UserDetails.shared.name = ((con as! DataSnapshot).value as! NSDictionary).value(forKey: "name") as! String
                    }
            }
                self.tableView.reloadData()
        }
            AppManager.shared.hideActivityIndicator(animated: true)
            
            //UserDetails.shared.arrangeIn()
        })
    }
    
    func getTotalNumberOfUnreadMessages(contactID : String, completionBlock: @escaping (_ succeeded: Int) -> Void) {
        let ref = rootRef.child("Chats/\(ContactListVC.UUID)/\(contactID)")
        ref.observe(.value, with: {(snapshot : DataSnapshot) in
            var count : Int = 0
            if snapshot.exists() {
                print("getTotalNumberOfUnreadMessages=\(snapshot)")
                for mes in snapshot.children {
                    if let received = ((mes as! DataSnapshot).value as! NSDictionary).value(forKey: "received") {
                        
                        if let isRead = ((mes as! DataSnapshot).value as! NSDictionary).value(forKey: "isRead") as? String{
                            
                            if isRead == "" {
                                
                                count = count + 1
                            }
                        }
                    }
                }
                print("count\(count)")
                completionBlock(count)
                
            }
            else {
                completionBlock(count)
            }
            
//            if snapshot.exists() {
//                var timeOfLastMessage : String = ""
//                var textMessage : String = ""
//                for mes in snapshot.children {
//                    let msg : message = message()
//                    count = count + 1
//                    print((mes as! DataSnapshot).key as String)
//                    msg.timeStamp = (mes as! DataSnapshot).key as String
//                    timeOfLastMessage = (mes as! DataSnapshot).key as String
//                    let timeStamp = Double(msg.timeStamp)
//                    let date = Date(timeIntervalSince1970: timeStamp!/1000)
//                    let formatter = DateFormatter()
//                    formatter.dateFormat = "yyyy/MM/dd"
//                    let timezone = TimeZone.current.abbreviation()
//                    print(timezone!)
//                    formatter.locale = Locale.current
//                    formatter.timeZone = TimeZone(abbreviation: timezone!)
//                    print(formatter.string(from: date))
//                    msg.date = formatter.string(from: date)
//                    print("date=\(msg.date)")
//                    formatter.dateFormat = "HH:mm"
//                    print("abcabc=\(date)")
//                    msg.time = formatter.string(from: date)
//                    print(formatter.string(from: date))
//
//                    if let sent = ((mes as! DataSnapshot).value as! NSDictionary).value(forKey: "sent") {
//                        msg.messageText = sent as! String
//                        textMessage = sent as! String
//                        msg.sender = true
//                        msg.receiver = false
//                    }
//                    if let received = ((mes as! DataSnapshot).value as! NSDictionary).value(forKey: "received") {
//                        msg.messageText = received as! String
//                        textMessage = received as! String
//                        msg.sender = false
//                        msg.receiver = true
//                    }
//                    //chatMessages.shared.messages.append(msg)
//                    print(count)
//                }
//
//
//            }
//            else {
//                AppManager.shared.hideActivityIndicator(animated: true)
//            }
        })
    }
    
    func getLastMessageWithTimeStamp(contactID : String, completionBlock: @escaping (_ succeeded: Dictionary<String, Any>) -> Void) {
        
        let ref = rootRef.child("Chats/\(ContactListVC.UUID)/\(contactID)")
        
        var messageHandler = ref.queryLimited(toLast: 1).observe(.value, with: { (snapshot : DataSnapshot) in
            var dictionary = Dictionary<String,Any>()
            print("snapshot321=\(snapshot)")
            print("snapshotValue = \(snapshot.value)")
            if snapshot.exists() {
                for message in snapshot.children
                {
                    var isSent : Bool = false
                    print((message as! DataSnapshot).key as String)
                    dictionary["lastMessageTimeStamp"] = (message as! DataSnapshot).key as String
                    if let sent = ((message as! DataSnapshot).value as! NSDictionary).value(forKey: "sent") {
                        dictionary["lastMessage"] = sent as! String
                        isSent = true
                        
                    }
                    if let received = ((message as! DataSnapshot).value as! NSDictionary).value(forKey: "received") {
                        dictionary["lastMessage"] = received as! String
                        isSent = false
                    }
                    if let read = ((message as! DataSnapshot).value as! NSDictionary).value(forKey: "isRead") as? String{
                        if read == "" {
                        dictionary["isRead"] = read
                            if isSent == true {
                                dictionary["isLastMessageSent"] = "true"
                            }
                            else {
                                dictionary["isLastMessageSent"] = "false"
                            }
                        }
                    }
                    if let read = ((message as! DataSnapshot).value as! NSDictionary).value(forKey: "isRead") as? NSNumber{
                        dictionary["isRead"] = read
                        print("readRead=\(read)")
                    }
                    completionBlock(dictionary)
                }
            }
            else {
                dictionary["lastMessageTimeStamp"] = ""
                dictionary["lastMessage"] = ""
                
                completionBlock(dictionary)
            }
            
            
            
        })
        //self.rootRef.child("Chats/\(ContactListVC.UUID)/\(contactID)").removeObserver(withHandle: messageHandler)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //UserDetails.shared.contacts.removeAll()
    }
    
    @IBAction func signOut(_ sender: UIBarButtonItem) {
        let logOut = UIAlertController(title: "", message: "Are you sure, you want to logout?", preferredStyle: UIAlertControllerStyle.alert)
        
        logOut.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            
            self.logout()
        }))
        
        logOut.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
        }))
        
        present(logOut, animated: true, completion: nil)
    }
    
    func logout() {
        // func for Signing the User Out
        if Auth.auth().currentUser != nil {
            do {
                try Auth.auth().signOut()
                UserDetails.shared.contacts.removeAll()
                chatMessages.shared.messages.removeAll()
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "FirstNavigationController")
                UIApplication.shared.keyWindow?.rootViewController = viewController;
                self.navigationController?.popToRootViewController(animated: true)
                self.rootRef.child("user_token/\(ContactListVC.UUID)").removeValue()
                
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        //return UserDetails.shared.contacts.count
        if self.filteredUserContacts.count == 0{
            return 0
        } else {
            return self.filteredUserContacts.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let contact = self.filteredUserContacts[indexPath.row]
        let cell: CustomCellContact = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! CustomCellContact
        cell.nameLabel.text = contact.name.capitalized
        cell.lastMessageLabel.text = contact.lastMessage
        if (contact.lastMessageTimeStamp) != "" {
            if let timeStamp = Double(contact.lastMessageTimeStamp){
                let date = Date(timeIntervalSince1970: timeStamp/1000)
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM//yyyy"
                let timezone = TimeZone.current.abbreviation()
                formatter.locale = Locale.current
                formatter.timeZone = TimeZone(abbreviation: timezone!)
                print(formatter.string(from: date))
                
                if formatter.string(from: date) != todayDate {
                    if (contact.lastMessageIsRead == "" && contact.isLastMessageSent == "false") {
                        cell.timeLabel.text = formatter.string(from: date)
                        cell.timeLabel.textColor = UIColor(red: 18/255, green: 146/255, blue: 255/255, alpha: 1.0)
                        cell.timeLabel.font = UIFont.boldSystemFont(ofSize: 15.0)
                        cell.lastMessageLabel.textColor = UIColor.black
                        cell.lastMessageLabel.font = UIFont.boldSystemFont(ofSize: 15.0)
                        if contact.unreadMessageCount == 0 {
                            cell.unreadMessageCount.isHidden = true
                           } else {
                            cell.unreadMessageCount.isHidden = false
                            cell.unreadMessageCount.layer.masksToBounds = true
                            cell.unreadMessageCount.layer.cornerRadius = 15
                            cell.unreadMessageCount.text = String(contact.unreadMessageCount)
                        }
                    } else {
                        cell.timeLabel.text = formatter.string(from: date)
                        cell.timeLabel.textColor = UIColor.gray
                        cell.timeLabel.font = UIFont.boldSystemFont(ofSize: 13.0)
                        cell.lastMessageLabel.textColor = UIColor.gray
                        cell.lastMessageLabel.font = UIFont.boldSystemFont(ofSize: 13.0)
                        print("PreviousDate= \(formatter.string(from: date))")
                        cell.unreadMessageCount.isHidden = true
                    }
                 } else {
                    formatter.dateFormat = "HH:mm"
                    print(contact.lastMessageIsRead)
                    print(contact.isLastMessageSent)
                    if (contact.lastMessageIsRead == "" && contact.isLastMessageSent == "false") {
                        cell.timeLabel.text = "Today \(formatter.string(from: date))"
                        cell.timeLabel.textColor = UIColor(red: 18/255, green: 146/255, blue: 255/255, alpha: 1.0)
                        cell.timeLabel.font = UIFont.boldSystemFont(ofSize: 15.0)
                        cell.lastMessageLabel.textColor = UIColor.black
                        cell.lastMessageLabel.font = UIFont.boldSystemFont(ofSize: 15.0)
                        if contact.unreadMessageCount == 0 {
                            cell.unreadMessageCount.isHidden = true
                        } else {
                            print(contact.unreadMessageCount)
                            cell.unreadMessageCount.isHidden = false
                            cell.unreadMessageCount.layer.masksToBounds = true
                            cell.unreadMessageCount.layer.cornerRadius = 15
                            cell.unreadMessageCount.text = String(contact.unreadMessageCount)
                        }
                    } else {
                        cell.timeLabel.text = "Today \(formatter.string(from: date))"
                        cell.timeLabel.textColor = UIColor.gray
                        cell.timeLabel.font = UIFont.boldSystemFont(ofSize: 13.0)
                        cell.lastMessageLabel.textColor = UIColor.gray
                        cell.lastMessageLabel.font = UIFont.boldSystemFont(ofSize: 13.0)
                        cell.unreadMessageCount.isHidden = true
                    }
                }
            }
        } else {
            cell.timeLabel.text = ""
            cell.lastMessageLabel.text = ""
            cell.unreadMessageCount.isHidden = true
        }
        return cell
    }
    
    var currentDate = String()
    var todayDate: String {
        get {
            let currDate = NSDate()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM//yyyy"
            let timezone = TimeZone.current.abbreviation()
            print(timezone!)
            formatter.locale = Locale.current
            formatter.timeZone = TimeZone(abbreviation: timezone!)
            currentDate = formatter.string(from: currDate as Date)
            return currentDate
        }
        //        set {
        //            privateDate = calendar.startOfDayForDate(newValue)
        //        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "showChats"
        {
            let vc = segue.destination as! ChatMessagesVC
            vc.contactID = selectedContactID
            vc.name = selectedContactName
            
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
