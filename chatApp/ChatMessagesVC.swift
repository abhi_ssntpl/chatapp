//
//  ChatMessagesVC.swift
//  chatApp
//
//  Created by Sword Software on 04/04/18.
//  Copyright © 2018 Sword Software. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase



class ChatMessagesVC: UIViewController, UITextViewDelegate, UITableViewDataSource, UITableViewDelegate{
    var dicArray = [Dictionary<String, String>]()
    @IBOutlet var textBox: UITextView!
    var rootRef : DatabaseReference!
    var messageHandler : DatabaseHandle?
    var messageHandler2 : DatabaseHandle?
    var name : String = ""
    var contactID : String = ""
    var dateArray : [String] = []
    var contact = Contact()
    var lastMessageSeenHandler : DatabaseHandle?
    
    var receiverToken : String = ""
    var chatMessagesDictionary: [Dictionary<String, Any>] = []
    var tempMessagesDictionary: [Dictionary<String, Any>] = []
    @IBOutlet var textViewBottomContraints: NSLayoutConstraint!
    @IBOutlet var textViewHeightContraint: NSLayoutConstraint!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var sendButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("ChatMessagesVC")
        rootRef = Database.database().reference()
        print(contactID)
        AppDelegate.openedChatContactId = contactID
        //contactID = ContactListVC.UUID
        self.textBox.delegate = self
        self.textBox.layer.borderWidth = 1
        self.textBox.layer.cornerRadius = 8
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        
        let contact_id = UserDefaults.standard.string(forKey: "contactID")
        print(contact_id)
        if contact_id != nil {
            self.contactID = contact_id!
        }
        let contact_name = UserDefaults.standard.string(forKey: "contactName")
        if contact_name != nil {
            self.title = contact_name
        } else {
            self.title = name
        }
        
        tableView.register(UINib(nibName: "TableViewCellForReceiver", bundle: nil), forCellReuseIdentifier: "Cell2")
        tableView.delegate = self as! UITableViewDelegate
        
        rootRef.child("user_token/\(contactID)").observe(.value, with: { (snapshot : DataSnapshot) in
            print("userToken=\(snapshot)")
            if snapshot.exists() {
            self.receiverToken = (snapshot.value as! NSDictionary).object(forKey: "token") as! String
            print("receiverToken\(self.receiverToken)")
            }
        })
        
        
//        tableView.scrollViewDidReachTop = { scrollView in
//            print("scrollViewDidReachTop")
//        }
//        tableView.scrollViewDidReachBottom = { scrollView in
//            print("scrollViewDidReachBottom")
//        }
        tableView.estimatedRowHeight = 56
        tableView.rowHeight = UITableViewAutomaticDimension
        
        sendButton.isEnabled = false
        NotificationCenter.default.addObserver(self, selector: #selector(ChatMessagesVC.keyboardShown), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        //tapGesture.cancelsTouchesInView = true
        view.addGestureRecognizer(tapGesture)
        if Reachability.isConnectedToNetwork() {
            //fetchingSentMessagesFromFirebase()
            AllMessagesFromReceiverEnd()
            fetchingNumberOfMessageFromFirebase()
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        chatMessages.shared.messages.removeAll()
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.scrollToBottom()
        self.sendButton.isEnabled = true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.textViewHeight(textView: textView)
    }
    @objc func keyboardShown(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        print("keyboardFrame: \(keyboardFrame.height)")
        self.textViewBottomContraints.constant = keyboardFrame.height
    }
    
    @objc func hideKeyboard() {
        self.textViewBottomContraints.constant = 10.0
        self.textBox.resignFirstResponder()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if self.messageHandler != nil {
            self.rootRef.removeAllObservers()
            self.rootRef.child("Chats/\(ContactListVC.UUID)/\(contactID)").removeObserver(withHandle: self.messageHandler!)
        }
         if self.messageHandler2 != nil {
            self.rootRef.child("Chats/\(contactID)/\(ContactListVC.UUID)").removeObserver(withHandle: self.messageHandler2!)
        }
        AppDelegate.openedChatContactId = ""
        let prefs = UserDefaults.standard
        prefs.removeObject(forKey: "contactID")
        prefs.removeObject(forKey: "contactName")
    }
    
    func textViewHeight(textView : UITextView) {
        let fixedWidth = textView.frame.size.width
        //  self.textViewHeightContraint.constant = CGFloat.greatestFiniteMagnitude
        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = textView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        
        // textView.textContainer.maximumNumberOfLines = 6
        
        if newSize.height >= 120 {
            self.textViewHeightContraint.constant = 120
            textView.frame.size.height = 120
            textView.isScrollEnabled = true
        } else {
            self.textViewHeightContraint.constant = newSize.height
            textView.frame.size.height = newSize.height
            textView.isScrollEnabled = false
        }
    }
    
    @IBAction func sendButtonPressed(_ sender: UIButton) {
        sendButton.isEnabled = false
        print("textBox.text\(textBox.text)")
        let time = (String(Int(Timestamp/1000)))
        print("time=\(time)")
         if !textBox.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty{
            let text = self.textBox.text
            print("text=\(text!)")
            self.textBox.text = ""
            let ref = rootRef.child("Chats/\(ContactListVC.UUID)")
            print(ref.child(time))
            ref.child(time).setValue(ServerValue.timestamp(), withCompletionBlock: {(_, ref: DatabaseReference) -> Void in
                self.fetchingFirebaseTimeStamp(time: time, text: text!)
            })
         } else {
            sendButton.isEnabled = true
            self.textViewHeightContraint.constant = 35.0
        }
//        if Reachability.isConnectedToNetwork(){
//        let time = (String(Int(Timestamp/1000)*1000))
//        print(time)
//        addSentMessageOnFirebase(timeStampString: time)
//        }
    }
    
    func fetchingFirebaseTimeStamp(time : String, text : String) {
        let ref = rootRef.child("Chats/\(ContactListVC.UUID)")
        ref.child(time).observeSingleEvent(of: .value, with: {(snapshot : DataSnapshot) in
            print("snapshot2=\(snapshot)")
            let timestamp = snapshot.value as! NSNumber
            print(timestamp)
            self.addSentMessageOnFirebase(text: text, timeStampString: timestamp)
            ref.child(time).removeValue()
        })
    }
    
//        func addSentMessageOnFirebase(timeStampString : NSNumber) {
//            rootRef.child("Chats/\(ContactListVC.UUID)/\(contactID)/\(timeStampString)/sent").setValue(textBox.text)
//            rootRef.child("Chats/\(contactID)/\(ContactListVC.UUID)/\(timeStampString)/received").setValue(textBox.text)
//            self.textBox.text = ""
//        }
    
    
    func addSentMessageOnFirebase(text : String ,timeStampString : NSNumber) {
        fireNotification(text: text)
        let messageDictionary = ["sent": text, "isRead": ""]
        let messageDictionary2 = ["received": text, "isRead": ""]
        rootRef.child("Chats/\(ContactListVC.UUID)/\(contactID)/\(timeStampString)").setValue(messageDictionary)
        rootRef.child("Chats/\(contactID)/\(ContactListVC.UUID)/\(timeStampString)").setValue(messageDictionary2)
        self.textViewHeightContraint.constant = 35.0
        sendButton.isEnabled = true
        //self.textViewBottomContraints.constant = 10.0
        //self.view.endEditing(true)
    }
    func fireNotification(text: String) {
        var dict1 = Dictionary<String,Any>()
        var dict2 = Dictionary<String,Any>()
        var dict3 = Dictionary<String,Any>()
        dict1["to"] = receiverToken
        dict1["priority"] = "high"
        dict2["title"] = UserDetails.shared.name
        dict2["body"] = "\(text)"
        dict2["content_available"] = "1"
        dict2["click_action"] = "https://development.ssntpl.com/Saurabh_mittal/chatapp/dashboard.html"
        dict2["icon"] = "http://development.ssntpl.com/Saurabh_mittal/chatapp/img/chatapp.png"
        dict3["sender_id"] = ContactListVC.UUID
        dict3["sender_name"] = UserDetails.shared.name
        dict1["notification"] = dict2
        dict1["data"] = dict3
        
        
//        var dict3 = Dictionary<String,Any>()
//        var dict2 = Dictionary<String,Any>()
//        dict2["title"] = UserDetails.shared.name.capitalized
//        dict2["text"] = "\(text)"
//        dict2["content_available"] = "1"
//        dict3["contactID"] = ContactListVC.UUID
//        dict3["click_action"] = "asas"
//        dict1["notification"] = dict2
//        dict1["data"] = dict3
//        dict1["to"] = receiverToken
//        dict1["priority"] = "high"
//        print("dict1\(dict1)")
        let data1 =  try! JSONSerialization.data(withJSONObject: dict1, options: .prettyPrinted)
        // first of all convert json to the data
        let convertedString = String(data: data1, encoding: String.Encoding.utf8)
        let LEGACY_SERVER_KEY = "AIzaSyCS2gtcQNe92eaUUaIL8dbCcrVIwVQkk6w"
        
        let json = convertedString?.data(using: .utf8)
        print("json\(json)")
        let url = NSURL(string: "https://fcm.googleapis.com/fcm/send")!
        let request = NSMutableURLRequest(url: url as URL)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("key=\(LEGACY_SERVER_KEY)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        
        request.httpBody = json
    let task = URLSession.shared.dataTask(with: request as URLRequest){ data,response,error in
            
            if error != nil{
                
                return
            }
        }
        task.resume()
    }
    
    var currentDate = String()
    var date: String {
        get {
            let currDate = NSDate()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/dd"
            let timezone = TimeZone.current.abbreviation()
            print(timezone!)
            formatter.locale = Locale.current
            formatter.timeZone = TimeZone(abbreviation: timezone!)
            currentDate = formatter.string(from: currDate as Date)
            return currentDate
        }
//        set {
//            privateDate = calendar.startOfDayForDate(newValue)
//        }
    }
    
    func scrollToBottom() {
       DispatchQueue.main.async {
            if chatMessages.shared.messages.count > 0 {
                let indexPath = IndexPath(row: chatMessages.shared.messages.count-1, section: 0)
                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
               // AppManager.shared.hideActivityIndicator(animated: true)
             }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let result: UITableViewCell
        print(indexPath.row)
        if chatMessages.shared.messages != nil && !chatMessages.shared.messages.isEmpty && chatMessages.shared.messages[indexPath.row].sender == true {
            let cell: TableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCell
            result = cell
            
            if(indexPath.row == 0) {
                if(chatMessages.shared.messages != nil && !chatMessages.shared.messages.isEmpty){
                    cell.dateLabel.isHidden = false
                    if (chatMessages.shared.messages[indexPath.row].date == date) {
                        cell.dateLabel.text = "Today"
                        
                    }
                    else { cell.dateLabel.text = chatMessages.shared.messages[indexPath.row].date
                    }
                }
            } else {
                if(chatMessages.shared.messages[indexPath.row-1].date != chatMessages.shared.messages[indexPath.row].date ) {
                    cell.dateLabel.isHidden = false
                    
                    if (chatMessages.shared.messages[indexPath.row].date == date) {
                        cell.dateLabel.text = "Today"
                    }
                    else { cell.dateLabel.text = chatMessages.shared.messages[indexPath.row].date
                    }
                }
                else {
                    cell.dateLabel.isHidden = true
                    //cell.labelContainerTopContraint.constant = 6
                }
            }
            // cell.timeReciever.isHidden = true
            cell.messageSender.isHidden = false
            cell.timeSender.isHidden = false
            cell.messageSender.text = chatMessages.shared.messages[indexPath.row].messageText
            cell.timeSender.text = chatMessages.shared.messages[indexPath.row].time
            if chatMessages.shared.messages[indexPath.row].isRead == "true" {
                cell.tickImageView.isHidden = false
                cell.tickImageView.image = UIImage(named: "tickImage")
            }
            else {
                cell.tickImageView.isHidden = true
            }
        } else {
            
            let cell: TableViewCellForReceiver = tableView.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath) as! TableViewCellForReceiver
            if(indexPath.row == 0) {
                if(chatMessages.shared.messages != nil && !chatMessages.shared.messages.isEmpty){
                    cell.dateLabel.isHidden = false
                    if (chatMessages.shared.messages[indexPath.row].date == date) {
                        cell.dateLabel.text = "Today"
                    }
                    else { cell.dateLabel.text = chatMessages.shared.messages[indexPath.row].date
                    }
                }
            } else {
                if(chatMessages.shared.messages != nil && !chatMessages.shared.messages.isEmpty && chatMessages.shared.messages[indexPath.row-1].date != chatMessages.shared.messages[indexPath.row].date ) {
                    cell.dateLabel.isHidden = false
                    if (chatMessages.shared.messages[indexPath.row].date == date) {
                        cell.dateLabel.text = "Today"
                    } else { cell.dateLabel.text = chatMessages.shared.messages[indexPath.row].date
                    }
                } else {
                    cell.dateLabel.isHidden = true
                    //cell.labelContainerTopContraint.constant = 6
                }
            }
            
            if chatMessages.shared.messages != nil && !chatMessages.shared.messages.isEmpty {
                cell.messageReceiver.text = chatMessages.shared.messages[indexPath.row].messageText
                cell.timeReceiver.text = chatMessages.shared.messages[indexPath.row].time
            }
            result = cell
        }
        return result
        
        //}
    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return dateArray[section]
//    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {

      //  return dates()
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        var rows: Int = 0
//        var array = [Int]()
//        array = dates3()
//        print("array=\(array)")
//        if(array.isEmpty) {
//            return 0
//        }
//
//       return array[section]
        
        return chatMessages.shared.messages.count
    }
    
    func dates()->Int {
        var count = chatMessages.shared.messages.count
        
        var array = [String]()
        for i in 0 ..< count {
            array.append(chatMessages.shared.messages[i].date)
        }
        dateArray = Array(Set(array))
        print(dateArray)
        count = Array(Set(array)).count
        print(count)
        return count
        
    }
    func dates3()->[Int] {
        var count = chatMessages.shared.messages.count
        print("count=\(count)")
        var cnt : Int = 1
        var array = [Int]()
        var firstDate = chatMessages.shared.messages[0].date
        
        for i in 1 ..< count {
            print(array)
            
        //    if(i < count) {
           
                if(firstDate == chatMessages.shared.messages[i].date) {
                    cnt = cnt + 1
                    firstDate = chatMessages.shared.messages[i].date
                    continue
                } else {
                    if (cnt > 1) {
                       array.append(cnt)
                        cnt = 1;
                    }
                }
         //   }
            array.append(cnt)
            print(array)
            print("cnt=\(cnt)")
            cnt = 0
        }
        return array
    }
    
//    func dates2()->[String] {
//        var count = chatMessages.shared.messages.count
//        var array = [String]()
//        for i in 0 ..< count {
//            array.append(chatMessages.shared.messages[i].date)
//        }
//        dateArray = Array(Set(array))
//        print(dateArray)
//        //count = Array(Set(array)).count
//        //print(count)
//        return dateArray
//
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath : IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        if tableView.isEditing {
            return .delete
        }
        return .none
    }
    
    func fetchingNumberOfMessageFromFirebase() {
        AppManager.shared.showActivityIndicator(withTitle: "loading")
        let ref = rootRef.child("Chats/\(ContactListVC.UUID)/\(contactID)")
        print(contactID)
        print("refref=\(ref)")
        self.messageHandler = ref.observe(.value, with: {(snapshot : DataSnapshot) in
            print("snapshot=\(snapshot)")
            var count = 0
            chatMessages.shared.messages.removeAll()
            if snapshot.exists() {
                for mes in snapshot.children {
                    print(mes)
                    let msg : message = message()
                    print((mes as! DataSnapshot).key as String)
                    msg.timeStamp = (mes as! DataSnapshot).key as String
                    let timeStamp = Double(msg.timeStamp)
                    let date = Date(timeIntervalSince1970: timeStamp!/1000)
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy/MM/dd"
                    let timezone = TimeZone.current.abbreviation()
                    print(timezone!)
                    formatter.locale = Locale.current
                    formatter.timeZone = TimeZone(abbreviation: timezone!)
                    print(formatter.string(from: date))
                    msg.date = formatter.string(from: date)
                    print("date=\(msg.date)")
                    formatter.dateFormat = "HH:mm"
                    msg.time = formatter.string(from: date)
                    print(formatter.string(from: date))

                    if let sent = ((mes as! DataSnapshot).value as! NSDictionary).value(forKey: "sent") {
                        msg.messageText = sent as! String
                        msg.sender = true
                        msg.receiver = false
                        
                        if let isRead = ((mes as! DataSnapshot).value as! NSDictionary).value(forKey: "isRead") as? NSNumber {
                            print("isRead=\(isRead)")
                            msg.isRead = "true"
                        }
                    }
                    else {
                        if let received = ((mes as! DataSnapshot).value as! NSDictionary).value(forKey: "received") {
                            msg.messageText = received as! String
                            msg.sender = false
                            msg.receiver = true
                            if let isRead = ((mes as! DataSnapshot).value as! NSDictionary).value(forKey: "isRead") as? String {
                                msg.isRead = isRead as! String
                                if msg.isRead == "" {
                                    print("Chats/\(ContactListVC.UUID)/\(self.contactID)/\(String(msg.timeStamp))/isRead")
                                    self.rootRef.child("Chats/\(ContactListVC.UUID)/\(self.contactID)/\(String(msg.timeStamp))/isRead").setValue(ServerValue.timestamp())
                                }
                            }
                            
                            if let isRead = ((mes as! DataSnapshot).value as! NSDictionary).value(forKey: "isRead") as? NSNumber {
                                print("isRead=\(isRead)")
                            }
                        }
                    }
                    chatMessages.shared.messages.append(msg)
                 }
                self.tableView.reloadData()
                AppManager.shared.hideActivityIndicator(animated: true)
                self.scrollToBottom()
            }
            else {
                AppManager.shared.hideActivityIndicator(animated: true)
            }
         })
    }

    //with childAdded Listerner latest 04/05/18
    func fetchingSentMessagesFromFirebase() {
        let ref = rootRef.child("Chats/\(ContactListVC.UUID)/\(contactID)")
        self.messageHandler = ref.observe(.childAdded, with: {(snapshot : DataSnapshot) in
            print("snapshot=\(snapshot)")
            //AppManager.shared.showActivityIndicator(withTitle: "Loading")
            let timeStamp = Double(snapshot.key)
            let msg : message = message()
            let date = Date(timeIntervalSince1970: timeStamp!/1000)
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/dd"
            let timezone = TimeZone.current.abbreviation()
            print(timezone!)
            formatter.locale = Locale.current
            formatter.timeZone = TimeZone(abbreviation: timezone!)
            print(formatter.string(from: date))
            msg.date = formatter.string(from: date)
            print("date=\(msg.date)")
            formatter.dateFormat = "HH:mm"
            print("abcabc=\(date)")
            msg.time = formatter.string(from: date)
            print(formatter.string(from: date))
            msg.timeStamp = snapshot.key

            if let sent = (snapshot.value as! NSDictionary).value(forKey: "sent") {
                msg.messageText = sent as! String
                msg.sender = true
                msg.receiver = false
                if let isRead = (snapshot.value as! NSDictionary).value(forKey: "isRead") as? NSNumber {
                    print("isRead=\(isRead)")
                    msg.isRead = "true"
                }
            } else {
                if let received = (snapshot.value as! NSDictionary).value(forKey: "received") {
                    msg.messageText = received as! String
                    msg.sender = false
                    msg.receiver = true

                    if let isRead = (snapshot.value as! NSDictionary).value(forKey: "isRead") as? String {
                        msg.isRead = isRead as! String
                        if msg.isRead == "" {
                            print(msg.messageText)
                            let Timestamp = String(snapshot.key)
                            print("Chats/\(ContactListVC.UUID)/\(self.contactID)/\(Timestamp)/isRead")
                            self.rootRef.child("Chats/\(ContactListVC.UUID)/\(self.contactID)/\(Timestamp)/isRead").setValue(ServerValue.timestamp())
                        }
                    }

                    if let isRead = (snapshot.value as! NSDictionary).value(forKey: "isRead") as? NSNumber {
                        print("isRead=\(isRead)")
                    }
                }
            }

            chatMessages.shared.messages.append(msg)
            self.tableView.reloadData()
            self.scrollToBottom()
        })
    }
    
    func AllMessagesFromReceiverEnd() {
        let ref = rootRef.child("Chats/\(contactID)/\(ContactListVC.UUID)")
        self.messageHandler2 = ref.observe(.childAdded, with: {(snapshot : DataSnapshot) in
            //print("snapshot=\(snapshot)")
            if let sent = (snapshot.value as! NSDictionary).value(forKey: "sent") {
                if let isRead = (snapshot.value as! NSDictionary).value(forKey: "isRead") as? String {
                    if isRead == "" {
                        let Timestamp = String(snapshot.key)
                        print("Chats/\(self.contactID)/\(ContactListVC.UUID)/\(Timestamp)/isRead")
                        self.rootRef.child("Chats/\(self.contactID)/\(ContactListVC.UUID)/\(Timestamp)/isRead").setValue(ServerValue.timestamp())
                    }
                }
                
                if let isRead = (snapshot.value as! NSDictionary).value(forKey: "isRead") as? NSNumber {
                    print("isRead=\(isRead)")
                }
            }
            if let received = (snapshot.value as! NSDictionary).value(forKey: "received") {
            }
//            if let isRead = (snapshot.value as! NSDictionary).value(forKey: "isRead") {
//                if (isRead as! String) == "" {
//                    let Timestamp = String(snapshot.key)
//                    print("Chats/\(ContactListVC.UUID)/\(self.contactID)/\(Timestamp)/isRead")
//                    self.rootRef.child("Chats/\(ContactListVC.UUID)/\(self.contactID)/\(Timestamp)/isRead").setValue(ServerValue.timestamp())
//                }
//            }
        })
    }
    
    
    //fetches only last 15 msgs
//    func fechingSentMessagesFromFirebase() {
//       let ref = rootRef.child("Chats/\(ContactListVC.UUID)/\(contactID)")
//        self.messageHandler = ref.queryLimited(toLast: 15).observe(.childAdded, with: {(snapshot : DataSnapshot) in
//            print("snapshot=\(snapshot)")
//            let timeStamp = Double(snapshot.key)
//            let msg : message = message()
//            let date = Date(timeIntervalSince1970: timeStamp!/1000)
//            let formatter = DateFormatter()
//            formatter.dateFormat = "yyyy/MM/dd"
//            let timezone = TimeZone.current.abbreviation()
//            print(timezone!)
//            formatter.locale = Locale.current
//            formatter.timeZone = TimeZone(abbreviation: timezone!)
//            print(formatter.string(from: date))
//            msg.date = formatter.string(from: date)
//            print("date=\(msg.date)")
//            formatter.dateFormat = "HH:mm"
//            print("abcabc=\(date)")
//
//            msg.time = formatter.string(from: date)
//            print(formatter.string(from: date))
//            msg.timeStamp = snapshot.key
//
//            if let sent = (snapshot.value as! NSDictionary).value(forKey: "sent") {
//                msg.messageText = sent as! String
//                msg.sender = true
//                msg.receiver = false
//            }
//            if let received = (snapshot.value as! NSDictionary).value(forKey: "received") {
//                msg.messageText = received as! String
//                msg.sender = false
//                msg.receiver = true
//            }
//            chatMessages.shared.messages.append(msg)
//            self.tableView.reloadData()
//            self.scrollToBottom()
//        })
//
//
//    }
    
    //    func fechingSentMessagesFromFirebase() {
    //        rootRef.child("Chats/\(ContactListVC.UUID)/\(contactID)").observeSingleEvent(of: .value, with: {(snapshot : DataSnapshot) in
    //            var messageDic = Dictionary<String, Any>()
    //            for message1 in snapshot.children {
    //                let date = (message1 as! DataSnapshot).key
    //                print("date=\(date)")
    //                var arrayOfMessageDictionary : [Dictionary<String, String>] = []
    //                //if date != ContactListVC.currentDateString {
    //                    messageDic["date"] = date
    //                    let dictionaryT = (message1 as! DataSnapshot)
    //
    //                    for mes in dictionaryT.children {
    //                        var messageDictionary: Dictionary<String, String> = [:]
    //                        let timeStamp = (mes as! DataSnapshot).key
    //                        messageDictionary["timeStamp"] = timeStamp
    //                        if let message = ((mes as! DataSnapshot).value as! NSDictionary).value(forKey: "sent") {
    //                            messageDictionary["messageText"] = message as? String
    //                            messageDictionary["sender"] = "true"
    //                            messageDictionary["received"] = "false"
    //                        }
    //                        if let message = ((mes as! DataSnapshot).value as! NSDictionary).value(forKey: "received") {
    //                            messageDictionary["messageText"] = message as? String
    //                            messageDictionary["sender"] = "false"
    //                            messageDictionary["received"] = "true"
    //                        }
    //                        arrayOfMessageDictionary.append(messageDictionary)
    //                    }
    //                    messageDic["messages"] = arrayOfMessageDictionary
    //               // }
    //                self.chatMessagesDictionary.append(messageDic)
    //            }
    //            print("chatMessagesDictionary=\(self.chatMessagesDictionary)")
    //            self.listenerForTodayMessage()
    //            //self.tableView.reloadData()
    //        })
    //    }
    
    func listenerForTodayMessage() {
        rootRef.child("Chats/\(ContactListVC.UUID)/\(contactID)").observe(.value, with: {(snapshot : DataSnapshot) in
            self.chatMessagesDictionary.removeLast()
            print("chatMessagesDictionary=\(self.chatMessagesDictionary)")
            var messageDic = Dictionary<String, Any>()
            for message1 in snapshot.children {
                let date = (message1 as! DataSnapshot).key
                print("date=\(date)")
                var arrayOfMessageDictionary : [Dictionary<String, String>] = []
                if date == ContactListVC.currentDateString {
                    messageDic["date"] = date
                    print("datedate=\(date)")
                    let dictionaryT = (message1 as! DataSnapshot)
                    
                    for mes in dictionaryT.children {
                        var messageDictionary: Dictionary<String, String> = [:]
                        let timeStamp = (mes as! DataSnapshot).key
                        messageDictionary["timeStamp"] = timeStamp
                        if let message = ((mes as! DataSnapshot).value as! NSDictionary).value(forKey: "sent") {
                            messageDictionary["messageText"] = message as? String
                            messageDictionary["sender"] = "true"
                            messageDictionary["received"] = "false"
                        }
                        if let message = ((mes as! DataSnapshot).value as! NSDictionary).value(forKey: "received") {
                            messageDictionary["messageText"] = message as? String
                            messageDictionary["sender"] = "false"
                            messageDictionary["received"] = "true"
                        }
                        print("messageDictionary=\(messageDictionary)")
                        arrayOfMessageDictionary.append(messageDictionary)
                    }
                    print("arrayOfMessageDictionary=\(arrayOfMessageDictionary)")
                    messageDic["messages"] = arrayOfMessageDictionary
                    print("messageDic=\(messageDic["messages"])")
                    
                    self.chatMessagesDictionary.append(messageDic)
                }
                
                
            }
            print(self.chatMessagesDictionary)
            self.tableView.reloadData()
        })
    }
    
    
    //    var messageDic = Dictionary<String, Any>()
    //    var arrayOfMessageDictionary : [Dictionary<String, String>] = []
    //    func listenerForTodayMessage() {
    //        self.messageDic["date"] = ContactListVC.currentDateString
    //        rootRef.child("Chats/\(ContactListVC.UUID)/\(contactID)/\(ContactListVC.currentDateString)").observe(.childAdded, with: { (snapshot : DataSnapshot) in
    //            var messageDictionary: Dictionary<String, String> = [:]
    //            print("addedListenerSnapshot = \(snapshot)")
    //            if let sentMesage = (snapshot.value as! NSDictionary).value(forKey: "sent") {
    //                messageDictionary["timeStamp"] = snapshot.key
    //                messageDictionary["messageText"] = sentMesage as? String
    //                messageDictionary["sender"] = "true"
    //                messageDictionary["received"] = "false"
    //                print("sentMessage=\(sentMesage)")
    //                self.arrayOfMessageDictionary.append(messageDictionary)
    //                print("arrayOfMessageDictionary=\(self.arrayOfMessageDictionary)")
    //            }
    //            if let receivedMesage = (snapshot.value as! NSDictionary).value(forKey: "received") {
    //                messageDictionary["timeStamp"] = snapshot.key
    //                messageDictionary["messageText"] = receivedMesage as? String
    //                messageDictionary["sender"] = "false"
    //                messageDictionary["received"] = "true"
    //                print("receivedMessage=\(receivedMesage)")
    //                self.arrayOfMessageDictionary.append(messageDictionary)
    //            }
    //
    //            self.messageDic["message"] = self.arrayOfMessageDictionary
    //            self.chatMessagesDictionary.append(self.messageDic)
    //            print(self.chatMessagesDictionary)
    ////            DispatchQueue.main.async(execute: {
    ////                self.tableView.reloadData()
    ////            })
    //        })
    //
    //        //self.tableView.reloadData()
    //    }
    
    
    
    func fechingRecievedMessagesFromFirebase() {
        rootRef.child("Chats/\(contactID)/\(ContactListVC.UUID)").observeSingleEvent(of: .value, with: {(snapshot : DataSnapshot) in
            print("snapshot=\(snapshot)")
        })
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ViewController: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("scrollView.contentOffset.y =", scrollView.contentOffset.y)
    }
}

