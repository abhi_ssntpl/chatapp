//
//  ViewController.swift
//  chatApp
//
//  Created by Sword Software on 03/04/18.
//  Copyright © 2018 Sword Software. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseMessaging


class ViewController: UIViewController {
    
    @IBOutlet var emailTF: UITextField!
    @IBOutlet var passwordTF: UITextField!
    var hidePassword: Bool!
    var rootRef : DatabaseReference!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("ViewController")
        rootRef = Database.database().reference()
        let emailImage = UIImageView(image: UIImage(named: "email_blue"))
        emailImage.frame = CGRect(x: 15, y: 7.5, width: emailImage.image!.size.width+10.0, height: emailImage.image!.size.height);
        emailImage.contentMode = UIViewContentMode.center
        self.emailTF.rightView = emailImage
        self.emailTF.rightViewMode = UITextFieldViewMode.always
        hidePassword = true
        passwordTF.isSecureTextEntry = true
        let keyImage = UIImageView(image: UIImage(named: "lock_blue"))
        keyImage.isUserInteractionEnabled = true
        keyImage.frame = CGRect(x: 15, y: 7.5, width: keyImage.image!.size.width+10.0, height: keyImage.image!.size.height);
        keyImage.contentMode = UIViewContentMode.center
        self.passwordTF.rightView = keyImage
        self.passwordTF.rightViewMode = UITextFieldViewMode.always
        // Do any additional setup after loading the view, typically from a nib.
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(KeyButtonPressed))
        keyImage.addGestureRecognizer(tapGestureRecognizer)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil);
        
    }
    
    @objc func KeyButtonPressed()
    {
        print("buttonPressed")
        if(hidePassword == true) {
            passwordTF.isSecureTextEntry = false
            hidePassword = false
        }
        else
        {
            passwordTF.isSecureTextEntry = true
            hidePassword = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func keyboardWillShow(_ sender: Notification)
    {
        self.view.frame.origin.y = -170
    }
    
    @objc func keyboardWillHide(_ sender: Notification)
    {
        self.view.frame.origin.y = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    @IBAction func signIn(_ sender: Any) {
        if (emailTF.text!.isEmpty || passwordTF.text!.isEmpty){
            let alert = UIAlertController(title: "Error", message: "Email or password is empty", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
        else {
            if Reachability.isConnectedToNetwork() {
                AppManager.shared.showActivityIndicator(withTitle: "Signing In")
                Auth.auth().signIn(withEmail: self.emailTF.text!, password: self.passwordTF.text!) { (user, error) in
                    if error == nil {
                        
                            //Print into the console if successfully logged in
                            print("You have successfully logged in")
                            let UUID = (user?.uid)!
                            print(UUID)
                            UserDefaults.standard.set(UUID, forKey: "userUUID")
                            UserDefaults.standard.synchronize()
                        
                        let token = Messaging.messaging().fcmToken
                        //print("token!=\(token!)")
                        
                        self.rootRef = Database.database().reference()
                        self.rootRef.child("user_token/\(UUID)/token").setValue(token)
                            
                            AppManager.shared.hideActivityIndicator(animated: true)
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "navigationContactPage")
                            UIApplication.shared.keyWindow?.rootViewController = viewController;
                     } else {
                        AppManager.shared.hideActivityIndicator(animated: true)
                        let A = UIAlertController(title: "Error", message: "\(error!.localizedDescription)", preferredStyle: .alert)
                        let B = UIAlertAction(title: "OK", style:UIAlertActionStyle.default, handler: nil)
                        A.addAction(B)
                        self.present(A, animated: true, completion: nil)
                     }
                }
            } else {
                let A = UIAlertController(title: "Connection error", message: "Internet connection must be available to sign in. Please try again later.", preferredStyle: .alert)
                let B = UIAlertAction(title: "OK", style:UIAlertActionStyle.default, handler: nil)
                A.addAction(B)
                self.present(A, animated: true, completion: nil)}
        }
        
    }
    
    @IBAction func register(_ sender: UIButton) {
        self.performSegue(withIdentifier: "registerSegue", sender: self)
    }
    @IBAction func forgotPassword(_ sender: UIButton) {
        self.performSegue(withIdentifier: "resetPasswordSegue", sender: self)
     }
    
}

