//
//  TableViewCellForReceiver.swift
//  chatApp
//
//  Created by Sword Software on 11/04/18.
//  Copyright © 2018 Sword Software. All rights reserved.
//

import UIKit

class TableViewCellForReceiver: UITableViewCell {
    @IBOutlet weak var labelContainerView: UIView!
    @IBOutlet var messageReceiver: UILabel!
    @IBOutlet var timeReceiver: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var labelContainerTopContraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelContainerView.layer.cornerRadius = 8
        labelContainerView.layer.masksToBounds = true
        
        dateLabel.layer.cornerRadius = 8
        dateLabel.layer.masksToBounds = true
        
        messageReceiver.layer.cornerRadius = 8
        messageReceiver.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
