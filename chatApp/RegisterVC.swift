//
//  RegisterVC.swift
//  chatApp
//
//  Created by Sword Software on 03/04/18.
//  Copyright © 2018 Sword Software. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class RegisterVC: UIViewController, UITextFieldDelegate {
    @IBOutlet var nameTF: UITextField!
    
    @IBOutlet var passwordTF: UITextField!
    @IBOutlet var emailTF: UITextField!
    var rootRef : DatabaseReference!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rootRef = Database.database().reference()
        self.navigationController?.isNavigationBarHidden = false
        self.title = "Register"
        self.passwordTF.isSecureTextEntry = true
        nameTF.delegate = self
        passwordTF.delegate = self
        emailTF.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        view.endEditing(true)
        self.view.frame.origin.y = 0
        super.touchesBegan(touches, with: event)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        
        if textField == self.nameTF
        {
            self.view.frame.origin.y = -80
        }
        if textField == self.emailTF
        {
            self.view.frame.origin.y = -90
        }
        if textField == self.passwordTF
        {
            self.view.frame.origin.y = -120
        }
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
        print("validate emilId: \(testStr)")
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: testStr)
    }
    
    @IBAction func register(_ sender: UIButton) {
        if emailTF.text!.isEmpty || passwordTF.text!.isEmpty || nameTF.text!.isEmpty{
            let A = UIAlertController(title: "ERROR !!!", message: "Email or password are empty", preferredStyle: .alert)
            let B = UIAlertAction(title: "OK", style:UIAlertActionStyle.default, handler: nil)
            A.addAction(B)
            present(A, animated: true, completion: nil)
        } else {
            if isValidEmail(testStr: self.emailTF.text!) {
                if Reachability.isConnectedToNetwork() {
                    AppManager.shared.showActivityIndicator(withTitle: "Registering")
                    Auth.auth().createUser(withEmail: emailTF.text!, password: passwordTF.text!) { (user, error) in
                        
                        if error == nil {
                            print("You have successfully signed up")
                            let uuid = user?.uid as! String
                            self.rootRef.child("Users/\(uuid)/email").setValue(self.emailTF.text)
                            self.rootRef.child("Users/\(uuid)/name").setValue(self.nameTF.text)
                            //Goes to the Setup page which lets the user take a photo for their profile picture and also chose a username
                            
                            self.popToSignInPage()
                            AppManager.shared.hideActivityIndicator(animated: true)
                        }
                        else {
                            AppManager.shared.hideActivityIndicator(animated: true)
                            let alertController = UIAlertController(title: "User already exists", message: error?.localizedDescription, preferredStyle: .alert)
                            
                            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                            alertController.addAction(defaultAction)
                            
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }
                }
                else {
                    let A = UIAlertController(title: "Connection error", message: "Internet connection must be available to register. Please try again later.", preferredStyle: .alert)
                    let B = UIAlertAction(title: "OK", style:UIAlertActionStyle.default, handler: nil)
                    A.addAction(B)
                    present(A, animated: true, completion: nil)
                    
                }
            }
            else {
                let A = UIAlertController(title: "Error", message: "Invalid email", preferredStyle: .alert)
                let B = UIAlertAction(title: "OK", style:UIAlertActionStyle.default, handler: nil)
                A.addAction(B)
                present(A, animated: true, completion: nil)
                
            }
        }
    }
    
    func popToSignInPage() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FirstNavigationController")
        self.present(vc!, animated: true, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
