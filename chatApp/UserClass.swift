//
//  UserClass.swift
//  chatApp
//
//  Created by Sword Software on 03/04/18.
//  Copyright © 2018 Sword Software. All rights reserved.
//

import Foundation
class UserDetails
{
    var id : String = ""
    var email : String = ""
    var name : String = ""
    var contacts: [Contact] = []
    static var shared = UserDetails()
    
    func arrangeIn() {
        contacts  = contacts.sorted { $1.lastMessageTimeStamp < $0.lastMessageTimeStamp}
    }
}
