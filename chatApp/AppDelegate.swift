//
//  AppDelegate.swift
//  chatApp
//
//  Created by Sword Software on 03/04/18.
//  Copyright © 2018 Sword Software. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import UserNotifications
import BRYXBanner

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate  {

    var window: UIWindow?
    static var openedChatContactId : String = "1"
    var contactID : String = ""

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        isUserAlreadyLoggedIn()
        UIApplication.shared.statusBarStyle = .lightContent
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        } else {
            // Fallback on earlier versions
        }
        self.initializeFCM(application)
        
        //if launchOptions
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("didFailToRegisterForRemoteNotificationsWithError: \(error)")
    }
    
    func application(received remoteMessage: MessagingRemoteMessage) {
        print("remoteMessage:\(remoteMessage.appData)")
    }
    
    
    func initializeFCM(_ application: UIApplication) {
        print("initializeFCM")
        //-------------------------------------------------------------------------//
        if #available(iOS 10.0, *) // enable new way for notifications on iOS 10
        {
            let center = UNUserNotificationCenter.current()
            center.delegate = self as! UNUserNotificationCenterDelegate
            center.requestAuthorization(options: [.badge, .alert , .sound]) { (accepted, error) in
                if !accepted
                {
                    print("Notification access denied.")
                }
                else
                {
                    print("Notification access accepted.")
                    UIApplication.shared.registerForRemoteNotifications();
                }
            }
        }
        else {
            let type: UIUserNotificationType = [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound];
            let setting = UIUserNotificationSettings(types: type, categories: nil);
            UIApplication.shared.registerUserNotificationSettings(setting);
            UIApplication.shared.registerForRemoteNotifications();
        }
        //FirebaseApp.configure()
        Messaging.messaging().delegate = self as! MessagingDelegate
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        debugPrint("didRegister notificationSettings")
        if (notificationSettings.types == .alert || notificationSettings.types == .badge || notificationSettings.types == .sound) {
            application.registerForRemoteNotifications()
        }
    }
    
    
//    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData)
//    {
//        debugPrint("didRegisterForRemoteNotificationsWithDeviceToken: NSDATA")
//
//        let token = String(format: "%@", deviceToken as CVarArg)
//        debugPrint("*** deviceToken: \(token)")
//        //        #if RELEASE_VERSION
//        //            InstanceID.instanceID().setAPNSToken(deviceToken as Data, type:FIRInstanceIDAPNSTokenType.prod)
//        //        #else
//        //            InstanceID.instanceID().setAPNSToken(deviceToken as Data, type:InstanceIDAPNSTokenType.sandbox)
//        //        #endif
//        Messaging.messaging().apnsToken = deviceToken as Data
//        debugPrint("Firebase Token:",InstanceID.instanceID().token() as! String)
//    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        debugPrint("didRegisterForRemoteNotificationsWithDeviceToken: DATA")
        let token = String(format: "%@", deviceToken as CVarArg)
        debugPrint("*** deviceToken: \(token)")
        //        #if RELEASE_VERSION
        //            FIRInstanceID.instanceID().setAPNSToken(deviceToken as Data, type:FIRInstanceIDAPNSTokenType.prod)
        //        #else
        //            InstanceID.instanceID().setAPNSToken(deviceToken as Data, type:InstanceIDAPNSTokenType.sandbox)
        //        #endif
        Messaging.messaging().apnsToken = deviceToken
        debugPrint("Firebase Token:",InstanceID.instanceID().token() as Any)
    }
    //-------------------------------------------------------------------------//
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        Messaging.messaging().appDidReceiveMessage(userInfo)
        print("In did Recieve Notification")
        // Print message ID.
        print("userInfoNotification=\(userInfo)")
        if let contactID = userInfo["sender_id"] as? String {
            self.contactID = contactID
            print(contactID)
        }
        
        let state = UIApplication.shared.applicationState
        if state == .active {
            print("App in Foreground")
             print("openedID=\(AppDelegate.openedChatContactId)")
            if self.contactID != AppDelegate.openedChatContactId {
                print("openedID=\(AppDelegate.openedChatContactId)")
                if let aps = userInfo["aps"] as? NSDictionary {
                    if let alert = aps["alert"] as? NSDictionary {
                        let body = alert["body"] as! String
                        let title = alert["title"] as! String
//                        var localNotification = UILocalNotification()
//                        localNotification.userInfo = userInfo
//                        localNotification.soundName = UILocalNotificationDefaultSoundName
//                        localNotification.alertBody = body
//                        localNotification.alertTitle = title
//                        localNotification.fireDate = Date()
//                        UIApplication.shared.scheduleLocalNotification(localNotification)
                        if body.characters.count > 50 {
                            let newBody = String(body.prefix(49))
                            let banner = Banner(title: title, subtitle: newBody, image: UIImage(named: "AppIcon"), backgroundColor: UIColor(red:31.00/255.0, green:136.0/255.0, blue:254.5/255.0, alpha:1.000))
                            banner.dismissesOnTap = true
                            banner.show(duration: 3.0)
                            //banner.didTapBlock:(() -> ())? = nil)
                        }
                        else{
                        let banner = Banner(title: title, subtitle: body, image: UIImage(named: "AppIcon"), backgroundColor: UIColor(red:31.00/255.0, green:136.0/255.0, blue:254.5/255.0, alpha:1.000))
                        banner.dismissesOnTap = true
                        banner.show(duration: 3.0)
                        }
                        
                    } else if let alert = aps["alert"] as? NSString {
                        
                    }
                }
            }
        }
        
        if state == .inactive || state == .background {
            var title : String = ""
            //let storyboard = UIStoryboard(name: "Main", bundle: nil)
            //var destinationViewController = storyboard.instantiateViewController(withIdentifier: "chatMessageVC") as! ChatMessagesVC
            //UserDefaults.standard.set(contactID, forKey: "contactID")
            //UserDefaults.standard.synchronize()
//            destinationViewController.contactID = self.contactID
//            let navigationController = self.window?.rootViewController as! UINavigationController
//            navigationController.pushViewController(destinationViewController, animated: false)
            if let aps = userInfo["aps"] as? NSDictionary {
                if let alert = aps["alert"] as? NSDictionary {
                    title = alert["title"] as! String
                }
            }
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "chatMessageVC") as! ChatMessagesVC
            viewController.contactID = self.contactID
            UserDefaults.standard.set(self.contactID, forKey: "contactID")
           UserDefaults.standard.set(title, forKey: "contactName")
            print("title\(title)")
            UIApplication.topViewController()?.navigationController?.pushViewController(viewController, animated: false)
            
        }
    }
    
//    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
//
//    }
    
    func application(_ application: UIApplication, shouldSaveApplicationState coder: NSCoder) -> Bool {
        return true
    }
    
    func application(_ application: UIApplication, shouldRestoreApplicationState coder: NSCoder) -> Bool {
        return true
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        Messaging.messaging().connect { error in
            print(error)
        }
    }
    
    func isUserAlreadyLoggedIn()
    {
        let userName = UserDefaults.standard.string(forKey: "userUUID")
        if userName != nil
        {
            OperationQueue.main.addOperation
                {
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "navigationContactPage")
                    UIApplication.shared.keyWindow?.rootViewController = viewController;
            }
        }
    }
    
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        Messaging.messaging().appDidReceiveMessage(userInfo)
        print(userInfo)
        print("UNUserNotificationCenterDelegate")
        print("In will Present notification")
        
        // Change this to your preferred presentation option
        completionHandler(.alert)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        print("InDidRecieveResponse")
        print("UNUserNotificationCenterDelegate")
        let userInfo = response.notification.request.content.userInfo
        Messaging.messaging().appDidReceiveMessage(userInfo)
        print(userInfo)
        
        completionHandler()
    }
}
// [END ios_10_message_handling]
extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        UserDefaults.standard.set("\(fcmToken)", forKey: "deviceToken")
        UserDefaults.standard.synchronize()
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}
extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}


