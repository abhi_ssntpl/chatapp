//
//  ResetPasswordVC.swift
//  PersonalRecordApp
//
//  Created by SwordMac11 on 28/02/18.
//  Copyright © 2018 SwordMac11. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class ResetPasswordVC: UIViewController,UITextFieldDelegate {
    
    
    @IBOutlet var emailTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        // Do any additional setup after loading the view.
        self.title = "Forgot password"
        emailTF.delegate = self
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
        self.view.frame.origin.y = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func resetButtonPressed(_ sender: UIButton) {
       self.view.frame.origin.y = 0
        if (emailTF.text?.isEmpty)! {
            let A = UIAlertController(title: "Error", message: "Email should not be empty", preferredStyle: .alert)
            let B = UIAlertAction(title: "OK", style:UIAlertActionStyle.default, handler: nil)
            A.addAction(B)
            present(A, animated: true, completion: nil)
        }
        else {
            if Reachability.isConnectedToNetwork() {
                AppManager.shared.showActivityIndicator(withTitle: "Wait")
                Auth.auth().sendPasswordReset(withEmail: self.emailTF.text!, completion: { (error) in
                    if error == nil{
                        
                        AppManager.shared.hideActivityIndicator(animated: true)
                        let alert = UIAlertController(title: "Mail has been sent", message: "Click on that link to change password", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                            
                            self.popToLogin()
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    } else {
                        print(error?.localizedDescription)
                    }
                    
                })
            }
            else {
                let A = UIAlertController(title: "Connection error", message: "Internet connection must be available to reset password. Please try again later.", preferredStyle: .alert)
                let B = UIAlertAction(title: "OK", style:UIAlertActionStyle.default, handler: nil)
                A.addAction(B)
                present(A, animated: true, completion: nil)
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if textField == self.emailTF
        {
            self.view.frame.origin.y = -100
        }
    }
    
    func popToLogin()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
