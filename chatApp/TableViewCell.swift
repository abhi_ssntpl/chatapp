//
//  TableViewCell.swift
//  chatApp
//
//  Created by Sword Software on 11/04/18.
//  Copyright © 2018 Sword Software. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var timeSender: UILabel!
    @IBOutlet var messageSender: UILabel!
    @IBOutlet weak var labelContainerView: UIView!
    @IBOutlet var labelContainerTopContraint: NSLayoutConstraint!
    
    @IBOutlet var tickImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        labelContainerView.layer.cornerRadius = 8
        labelContainerView.layer.masksToBounds = true
        
        dateLabel.layer.cornerRadius = 8
        dateLabel.layer.cornerRadius = 8
        
        messageSender.layer.cornerRadius = 8
        messageSender.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
