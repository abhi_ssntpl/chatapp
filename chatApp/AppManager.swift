//
//  AppManager.swift
//  GogoApp
//
//  Created by SwordMac11 on 07/12/17.
//  Copyright © 2017 SwordMac11. All rights reserved.
//

import Foundation

class AppManager
{
    var activityIndicator = MBProgressHUD()
    static var shared = AppManager()
//
//        - (void)showActivityIndicatorWithTitle:(NSString*)title {
//    [self hideActivityIndicatorAnimated:NO];
//    progressIndicatorView = [MBProgressHUD showHUDAddedTo:[[[UIApplication sharedApplication] delegate] window] animated:YES];
//    [progressIndicatorView.layer setBackgroundColor:[[UIColor colorWithWhite: 0.20 alpha:0.50] CGColor]];
//    progressIndicatorView.mode = MBProgressHUDModeIndeterminate;
//    progressIndicatorView.labelText = title?:@"";
//    progressIndicatorView.labelFont = FONT_SIZE;
//    }
    
    
//        - (void)showActivityIndicatorWithTitle:(NSString*)title {
//    [self hideActivityIndicatorAnimated:NO];
//    progressIndicatorView = [MBProgressHUD showHUDAddedTo:[[[UIApplication sharedApplication] delegate] window] animated:YES];
//    [progressIndicatorView.layer setBackgroundColor:[[UIColor colorWithWhite: 0.20 alpha:0.50] CGColor]];
//    progressIndicatorView.mode = MBProgressHUDModeIndeterminate;
//    progressIndicatorView.labelText = title?:@"";
   // progressIndicatorView.labelFont = FONT_SIZE;
//    }
    
//    - (void)hideActivityIndicatorAnimated:(BOOL)animated {
//    [MBProgressHUD hideAllHUDsForView:[[[UIApplication sharedApplication] delegate] window] animated:animated];
//    progressIndicatorView = nil;
//    }
    
    
    
    func hideActivityIndicator(animated:Bool) {
        MBProgressHUD.hideAllHUDs(for: (UIApplication.shared.delegate?.window)!, animated: true)
       
    }
    
    func showActivityIndicator(withTitle title: String) {
        hideActivityIndicator(animated: false)
        activityIndicator = MBProgressHUD.showAdded(to: (UIApplication.shared.delegate?.window)!, animated: true)
        activityIndicator.layer.backgroundColor = UIColor(white: 0.20, alpha: 0.50).cgColor
        activityIndicator.mode = MBProgressHUDMode.indeterminate
        activityIndicator.labelText = title ?? ""
        
    }
    
    
    func showActivityIndicatorWithoutBox(withTitle title: String) {
        hideActivityIndicator(animated: false)
        activityIndicator = MBProgressHUD.showAdded(to: (UIApplication.shared.delegate?.window)!, animated: true)
        activityIndicator.color = UIColor.clear
        activityIndicator.layer.backgroundColor = UIColor(white: 0.20, alpha: 0.50).cgColor
        activityIndicator.mode = MBProgressHUDMode.indeterminate
        activityIndicator.labelText = title ?? ""
        
    }
    
    func updateActivityIndicatorTitle(_ title: String) {
        if activityIndicator != nil{
            activityIndicator.labelText = title ?? ""
        }
        else {
            showActivityIndicator(withTitle: title)
        }
    }


}
