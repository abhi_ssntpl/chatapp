//
//  CustomCellContact.swift
//  chatApp
//
//  Created by Sword Software on 03/04/18.
//  Copyright © 2018 Sword Software. All rights reserved.
//

import UIKit

class CustomCellContact: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var lastMessageLabel: UILabel!
    @IBOutlet var timeLabel:  UILabel!

    @IBOutlet var unreadMessageCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        
        //set cell to initial state here, reset or set values, etc.
    }

}
